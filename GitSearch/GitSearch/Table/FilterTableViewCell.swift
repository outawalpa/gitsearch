//
//  FilterTableViewCell.swift
//  GitSearch
//
//  Created by Ob'dO Contact Agile on 29/08/2019.
//  Copyright © 2019 CarlDelacour. All rights reserved.
//

import UIKit

class FilterTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLanguage: UILabel!
    @IBOutlet weak var imageState: UIImageView!
    
    // MARK: - Static attributes
    static let NibName = "FilterTableViewCell"
    static let Identifier = "filterCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

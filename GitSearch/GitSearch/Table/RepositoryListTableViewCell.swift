//
//  RepositoryListTableViewCell.swift
//  GitSearch
//
//  Created by Ob'dO Contact Agile on 29/08/2019.
//  Copyright © 2019 CarlDelacour. All rights reserved.
//

import UIKit

class RepositoryListTableViewCell: UITableViewCell {

    @IBOutlet weak var nameProject: UILabel!
    @IBOutlet weak var imageLanguage: UIImageView!
    
    // MARK: - Static attributes
    static let NibName = "RepositoryListTableViewCell"
    static let Identifier = "repositoryListCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

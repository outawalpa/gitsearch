//
//  JsonReader.swift
//  GitSearch
//
//  Created by Ob'dO Contact Agile on 28/08/2019.
//  Copyright © 2019 CarlDelacour. All rights reserved.
//

import Foundation

//Read json 
struct Repository: Decodable {
    let items: [Items]
    let total_count: Int
    
}

struct Items: Decodable {
    let created_at: String?
    let description: String?
    let language: String?
    let name: String?
    let owner: Owner?
}

struct Owner: Decodable {
    let avatar_url: String?
    let login: String?
}

//
//  Language.swift
//  GitSearch
//
//  Created by Ob'dO Contact Agile on 28/08/2019.
//  Copyright © 2019 CarlDelacour. All rights reserved.
//

import Foundation

//List of language use in github
public enum language: String {
    case assembly = "assembly" 
    case c = "c"
    case cSharp = "csharp"
    case cPlus = "cplus"
    case css = "css"
    case go = "go"
    case groovy = "groovy"
    case html = "html"
    case java = "java"
    case javaScript = "javascript"
    case kotlin = "kotlin"
    case objective = "objective"
    case objectivePlus = "objective-C"
    case php = "php"
    case python = "python"
    case rtf = "richtextformat"
    case ruby = "ruby"
    case rust = "rust"
    case shell = "shell"
    case swift = "swift"
    case typeScript = "typescript"
    case vimScript = "vimscript"
    case vue = "vue"
}

/**
 Return a readable string for language
 */
public func languageReadable(_ language: language) -> String {
    switch language {
    case .assembly:
        return "Assembleur"
    case .c:
        return "C"
    case .cSharp:
        return "C#"
    case .cPlus:
        return "C++"
    case .css:
        return "CSS"
    case .go:
        return "GO"
    case .groovy:
        return "Groovy"
    case .html:
        return "HTML"
    case .java:
        return "Java"
    case .javaScript:
        return "JavaScript"
    case .kotlin:
        return "Kotlin"
    case .objective:
        return "Objective C"
    case .objectivePlus:
        return "Objective C++"
    case .php:
        return "PHP"
    case .python:
        return "Python"
    case .rtf:
        return "Rich Text Format"
    case .ruby:
        return "Ruby"
    case .rust:
        return "Rust"
    case .shell:
        return "Shell"
    case .swift:
        return "Swift"
    case .typeScript:
        return "Type Script"
    case .vimScript:
        return "Vim Script"
    case .vue:
        return "Vue"
    }
}

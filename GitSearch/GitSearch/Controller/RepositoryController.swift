//
//  RepositoryController.swift
//  GitSearch
//
//  Created by Ob'dO Contact Agile on 28/08/2019.
//  Copyright © 2019 CarlDelacour. All rights reserved.
//

import Foundation
import UIKit

class RepositoryController: UIViewController {
    

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var photo: UIImageView!
    
    var name: String = ""
    var date: String = ""
    var language: String = ""
    var descript: String = ""
    var login: String = ""
    var url: String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = name
        
        dateLabel.text = date
        languageLabel.text = language
        descriptionLabel.text = descript
        loginLabel.text = login
        
        let link = URL(string: url)
        let data = try? Data(contentsOf: link!)
        photo.image = UIImage(data: data!)
    }
    
    
}

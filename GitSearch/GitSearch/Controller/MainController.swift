//
//  ViewController.swift
//  GitSearch
//
//  Created by Ob'dO Contact Agile on 26/08/2019.
//  Copyright © 2019 CarlDelacour. All rights reserved.
//

import UIKit
import Alamofire
import Spring

class MainController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, LanguageListDelegate {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var listGit: UITableView!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var pageLabel: UILabel!
    @IBOutlet weak var waitingView: UIView!
    @IBOutlet weak var waitingImage: SpringImageView!
    
    var list: [Items] = []
    var name: String = ""
    var languageSelected: [language] = []
    
    var selectedRepository: Int = 0
    
    var animationTimer = Timer()
    
    var page: Int = 0 {
        didSet {
            pageLabel.text = "Page \(self.page + 1)/\(self.pageMax + 1)"
        }
    }
    var pageMax: Int = 0 {
        didSet {
            pageLabel.text = "Page \(self.page + 1)/\(self.pageMax + 1)"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Parameter views
        waitingView.backgroundColor = UIColor(displayP3Red: 0, green: 0, blue: 0, alpha: 0.2)
        waitingView.isHidden = true
        
        
        //Table
        let repositoryTableNib = UINib(nibName: RepositoryListTableViewCell.NibName, bundle: nil)
        listGit.register(repositoryTableNib, forCellReuseIdentifier: RepositoryListTableViewCell.Identifier)
        
        listGit.delegate = self
        listGit.dataSource = self
        
        
        //Gesture recognizer
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeLeft.direction = .left
        self.view!.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.view!.addGestureRecognizer(swipeRight)
        
        //Text field and keyboard
        nameTextField.delegate = self
    }
    
    @IBAction func nameSet(_ sender: Any) {
        name = nameTextField.text ?? ""
    }
    
    @IBAction func languageFilter(_ sender: Any) {
        if let filter = self.storyboard?.instantiateViewController(withIdentifier: LanguageListController.IDENTIFIER) as? LanguageListController {
            filter.delegate = self
            filter.actualList = self.languageSelected
            self.present(filter, animated: true)
        }
    }
    
    @IBAction func searchAction(_ sender: Any) {
        page = 0
        search()
    }
    
    //MARK: -Alamofire request
    func search() {
        
        //Launch waiting view with animation
        self.waitingView.isHidden = false
        self.animationTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.animation), userInfo: nil, repeats: true)
        
        //Async for display waiting view
        DispatchQueue.main.async {
            
            //Create path with selected language
            var languagePath = ""
            for i in 0..<self.languageSelected.count {
                languagePath += "+language:\(self.languageSelected[i])"
            }
            
            //The request
            Alamofire.request("https://api.github.com/search/repositories?q=+user:\(self.name)\(languagePath)&page=\(self.page+1)&per_page=100&sort=stars&order=desc", method: .get).responseJSON { response in
                if response.result.isSuccess {
                    guard let data = response.data else { return }
                    
                    do {
                        self.list = []
                        
                        let myResponse = try JSONDecoder().decode(Repository.self, from: data)
                        for item in myResponse.items {
                            self.list.append(item)
                        }
                        self.resultLabel.text = "\(myResponse.total_count) Resultat\(myResponse.total_count > 1 ? "s":"")"
                        
                        self.pageMax = myResponse.total_count / 100
                        
                        self.listGit.reloadData()
                        
                        self.waitingView.isHidden = true
                        self.animationTimer.invalidate()
                    }
                    catch{
                        let alert = UIAlertController(title: "Impossible", message: "Aucun répertoire ne correspond à vos critères de recherche.", preferredStyle: .alert)
                        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
                        alert.addAction(action)
                        self.present(alert, animated: true)
                        
                        self.waitingView.isHidden = true
                        self.animationTimer.invalidate()
                    }
                }
                else {
                    
                    let alert = UIAlertController(title: "Erreur", message: "Une erreur est survenue, veuillez nous excuser pour la gêne occasionnée", preferredStyle: .alert)
                    let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    alert.addAction(action)
                    self.present(alert, animated: true)
                    
                    self.waitingView.isHidden = true
                    self.animationTimer.invalidate()
                }
                
                //            if let result = response.result.value {
                //                print("JSON: \(result)") // serialized json response
                //            }
            }
        }
        
    }
    
    //Check gesture
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizer.Direction.left {
            if (page < pageMax) && waitingView.isHidden {
                self.page += 1
                self.search()
            }
        }
        if gesture.direction == UISwipeGestureRecognizer.Direction.right {
            if (page > 0) && waitingView.isHidden {
                self.page -= 1
                self.search()
            }
        }
    }
    
    //Animate image with spring
    @objc func animation()
    {
        self.waitingImage.animation = "swing"
        self.waitingImage.curve = "easeOut"
        self.waitingImage.duration = 1
        self.waitingImage.animate()
    }

    
    //MARK: -TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RepositoryListTableViewCell.Identifier, for: indexPath) as! RepositoryListTableViewCell
        
        cell.nameProject.text = "\(list[indexPath.row].name ?? "##### ERROR #####")"
        cell.imageLanguage.image = UIImage(named: list[indexPath.row].language ?? "none")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedRepository = indexPath.row
        self.performSegue(withIdentifier: "repositorySegue", sender: self)
    }
    
    
    //MARK: -Delegate
    func updateList(_ list: [language]) {
        languageSelected = list
    }
    
    
    //Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? RepositoryController {
            
            if let name = list[selectedRepository].name {
                destination.name = name
            }
            if let date = list[selectedRepository].created_at {
                destination.date = date
            }
            if let language = list[selectedRepository].language {
                destination.language = language
            }
            if let description = list[selectedRepository].description{
                destination.descript = description
            }
            if let login = list[selectedRepository].owner?.login {
                destination.login = login
            }
            if let url = list[selectedRepository].owner!.avatar_url {
                destination.url = url
            }
        }
    }
    
    //Keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}


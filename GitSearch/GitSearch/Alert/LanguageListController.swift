//
//  LanguageListController.swift
//  GitSearch
//
//  Created by Ob'dO Contact Agile on 28/08/2019.
//  Copyright © 2019 CarlDelacour. All rights reserved.
//

import Foundation
import UIKit

class LanguageListController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    //MARK: -Constants
    public static let IDENTIFIER = "languageList"
    
    //MARK: -Controls
    @IBOutlet weak var table: UITableView!
    
    //MARK: -Delegate
    var delegate: LanguageListDelegate?
    
    var actualList: [language] = []
    let totalLanguage: [language] = [.assembly, .c, .cPlus, .cSharp, .css, .go, .groovy, .html, .java, .javaScript, .kotlin, .objective, .objectivePlus, .php, .python, .rtf, .ruby, .rust, .shell, .swift, .typeScript, .vimScript, .vue]
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //Table
        let filterTableNib = UINib(nibName: FilterTableViewCell.NibName, bundle: nil)
        table.register(filterTableNib, forCellReuseIdentifier: FilterTableViewCell.Identifier)
        
        table.delegate = self
        table.dataSource = self
        
    }
    
    
    //MARK: -Actions
    @IBAction func doneAction(_ sender: Any) {

        self.dismiss(animated: true, completion: {
            self.delegate?.updateList(self.actualList)
        })
    }
    
    //MARK: -Table
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return totalLanguage.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FilterTableViewCell.Identifier, for: indexPath) as! FilterTableViewCell
        
        cell.nameLanguage.text = languageReadable(totalLanguage[indexPath.row])
        
        if actualList.contains(totalLanguage[indexPath.row]) {
            cell.imageState.image = UIImage(named: "check")
        } else {
            cell.imageState.image = UIImage(named: "uncheck")
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if actualList.contains(totalLanguage[indexPath.row]) {
            actualList.remove(at: actualList.firstIndex(of: totalLanguage[indexPath.row])!)
        } else {
            actualList.append(totalLanguage[indexPath.row])
        }
        
        table.reloadData()
    }
    
}

protocol LanguageListDelegate
{
    /**
     Called when tasks have been cancelled by the user.
     */
    func updateList(_ list: [language])
    
}

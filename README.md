<h3>XCODE </h3>
<p>Ouvrir le fichier "Workspace", branché un téléphone, le selectionner dans la liste en haut à gauche comme cible et lancer l'application.</p>
<p>Si une erreur survient, il s'agit surement de la signature de l'application, cliquez sur l'icone bleu "GitSearch" en haut à gauche :<br>
Général > Team > Mettre votre compte développeur apple.</p>
<p>Si ce n'est pas encore fait il faudra accepter le compte apple dev dans l'iphone.</p>
</br>
<p>Si cela ne marche pas et que l'erreur provient d'un pod, voir l'étape **COCOAPODS**.</p>

<h3>COCOAPODS</h3>
<p>Si cocoapods n'est pas installé sur la machine :<br>
Dans la console : `$ sudo gem install cocoapods`</p>
<p>Puis rendez vous à la racine du fichier où se trouve le ficher "Podfile"<br>
Dans la console : `$ pod install`</p>